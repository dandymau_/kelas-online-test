<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class KelasModel extends Model
{
    use SoftDeletes;

    protected $table = 'kelas';

    protected $fillable = [
        'nama_kelas',
        'pengajar',
        'kategori',
        'trailer',
        'deskripsi'
    ];

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    public function pengajar_kelas() {
        return $this->belongsTo('App\Models\PengajarModel', 'pengajar');
    }

    public function kategori_kelas() {
        return $this->belongsTo('App\Models\KategoriModel', 'kategori');
    }
}
