<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PengajarModel extends Model
{
    use SoftDeletes;

    protected $table = 'pengajar';

    protected $fillable = [
        'nama_pengajar'
    ];

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    public function kelas() {
        return $this->hasMany('App\Models\KelasModel');
    }
}
