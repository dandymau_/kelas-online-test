<?php

namespace App\Http\Controllers\AdminController;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\PengajarModel as Pengajar;
use DB;

class PengajarController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.tambah-pengajar');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'nama_pengajar' => 'required|max:255'
        ]);

        if ($validator->fails()) {
            return back()->with([
                'errors' => $validator->errors()
            ]);
        }

        DB::beginTransaction();
        try {
            $insert = Pengajar::create([
                'nama_pengajar' => $request->input('nama_pengajar')
            ]);
        } catch(Exception $e) {
            DB::rollback();
            return back()->with([
                'error' => 'Gagal menambahkan data. ErrMsg: ' . $e->getMessage()
            ]);
        }

        DB::commit();
        return redirect(route('dashboard'))->with([
            'success' => 'Berhasil menambahkan data'
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Pengajar::find($id);

        return view('admin.edit-pengajar')->with([
            'pengajar' => $data
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'nama_pengajar' => 'required|max:255'
        ]);

        if ($validator->fails()) {
            return back()->with([
                'errors' => $validator->errors()
            ]);
        }

        DB::beginTransaction();
        try {
            $insert = Pengajar::where('id', $id)
                        ->update([
                            'nama_pengajar' => $request->input('nama_pengajar')
                        ]);
        } catch(Exception $e) {
            DB::rollback();
            return back()->with([
                'error' => 'Gagal mengubah data. ErrMsg: ' . $e->getMessage()
            ]);
        }

        DB::commit();
        return redirect(route('dashboard'))->with([
            'success' => 'Berhasil mengubah data'
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = Pengajar::find($id);

        DB::beginTransaction();
        try {
            $data->delete();
        } catch(Exception $e) {
            DB::rollback();
            return back()->with([
                'error' => 'Gagal menghapus data. ErrMsg: ' . $e->getMessage()
            ]);
        }

        DB::commit();
        return back()->with([
            'success' => 'Berhasil menghapus data'
        ]);
    }
}
