<?php

namespace App\Http\Controllers\AdminController;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Models\KelasModel as Kelas;
use App\Models\KategoriModel as Kategori;
use App\Models\PengajarModel as Pengajar;
use DB;

class KelasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data_pengajar = Pengajar::all();
        $data_kategori = Kategori::all();

        return view('admin.tambah-kelas')->with([
            'data_pengajar' => $data_pengajar,
            'data_kategori' => $data_kategori
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'nama_kelas' => 'max:255|required',
            'pengajar' => 'required',
            'kategori' => 'required',
            'trailer' => 'mimes:mp4'
        ]);

        if ($validator->fails()) {
            return back()->with([
                'errors' => $validator->errors()
            ]);
        }

        DB::beginTransaction();
        try {
            $data = [
                'nama_kelas' => $request->input('nama_kelas'),
                'pengajar' => $request->input('pengajar'),
                'kategori' => $request->input('kategori'),
                'deskripsi' => $request->input('deskripsi')
            ];
            
            if ($request->hasFile('trailer')) {
                $file_name = 'trailer-' . str_replace(' ', '', $request->input('nama_kelas')) . date('Hi') . '.' . $request->trailer->extension();
                $path = 'storage/' . $file_name;
                $upload = $request->trailer->storeAs('public', $file_name);
                $data['trailer'] = $path;
            }

            $insert = Kelas::create($data);
        } catch (Exception $e) {
            DB::rollback();
            return back()->with([
                'error' => 'Gagal tambah data. ErrMsg' . $e->getMessage()
            ]);
        }

        DB::commit();
        return redirect(route('dashboard'))->with([
            'success' => 'Berhasil menambahkan kelas'
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Kelas::find($id);
        $data_pengajar = Pengajar::all();
        $data_kategori = Kategori::all();

        return view('admin.edit-kelas')->with([
            'kelas' => $data,
            'data_pengajar' => $data_pengajar,
            'data_kategori' => $data_kategori
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'nama_kelas' => 'max:255|required',
            'pengajar' => 'required',
            'kategori' => 'required',
            'trailer' => 'mimes:mp4'
        ]);

        if ($validator->fails()) {
            return back()->with([
                'errors' => $validator->errors()
            ]);
        }

        DB::beginTransaction();
        try {
            $data = [
                'nama_kelas' => $request->input('nama_kelas'),
                'pengajar' => $request->input('pengajar'),
                'kategori' => $request->input('kategori'),
                'deskripsi' => $request->input('deskripsi')
            ];
            
            if ($request->hasFile('trailer')) {
                $file_name = 'trailer-' . str_replace(' ', '', $request->input('nama_kelas')) . date('Hi') . '.' . $request->trailer->extension();
                $path = 'storage/' . $file_name;
                $upload = $request->trailer->storeAs('public', $file_name);
                $data['trailer'] = $path;
            }

            $insert = Kelas::where('id', $id)
                        ->update($data);
        } catch (Exception $e) {
            DB::rollback();
            return back()->with([
                'error' => 'Gagal tambah data. ErrMsg' . $e->getMessage()
            ]);
        }

        DB::commit();
        return redirect(route('dashboard'))->with([
            'success' => 'Berhasil mengubah data kelas'
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = Kelas::find($id);

        DB::beginTransaction();
        try {
            $delete = $data->delete();
        } catch(Exception $e) {
            DB::rollback();
            return back()->with([
                'error' => 'Gagal hapus data. ErrMsg: ' . $e->getMessage()
            ]);
        }

        DB::commit();
        return redirect(route('dashboard'))->with([
            'success' => 'Berhasil hapus data'
        ]);
    }
}
