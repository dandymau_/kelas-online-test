<?php

namespace App\Http\Controllers\AdminController;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\KelasModel as Kelas;
use App\Models\PengajarModel as Pengajar;
use App\Models\KategoriModel as Kategori;
use Datatables;
use DB;

class DashboardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.dashboard');
    }

    public function dataKelas(Request $request) {
        $data = Kelas::all();
        return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function($data) {
                    return '<a href="'. route('edit-kelas', ['id' => $data->id]) .'" class="btn btn-warning btn-sm">Edit</a> ' . 
                    '<a href="'. route('hapus-kelas', ['id' => $data->id]) .'" class="btn btn-danger btn-sm">Delete</a>'
                    
                    ;
                })
                ->addColumn('nama_pengajar', function($kelas) {
                    return $kelas->pengajar_kelas->nama_pengajar;
                })
                ->removeColumn('pengajar')
                ->make(true);
    }

    public function dataPengajar(Request $request) {
        $data = Pengajar::all();
        return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function($data) {
                    return '<a href="'. route('edit-pengajar', ['id' => $data->id]) .'" class="btn btn-warning btn-sm">Edit</a> ' . 
                    '<a href="'. route('hapus-pengajar', ['id' => $data->id]) .'" class="btn btn-danger btn-sm">Delete</a>'
                    
                    ;
                })
                ->make(true);
    }

    public function dataKategori(Request $request) {
        $data = Kategori::all();
        return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function($data) {
                    return '<a href="'. route('edit-kategori', ['id' => $data->id]) .'" class="btn btn-warning btn-sm">Edit</a> ' . 
                    '<a href="'. route('hapus-kategori', ['id' => $data->id]) .'" class="btn btn-danger btn-sm">Delete</a>'
                    
                    ;
                })
                ->make(true);
    }
}
