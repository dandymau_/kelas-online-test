<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group([
    'prefix' => 'admin-page'
], function($router) {
    Route::get('/', 'AdminController\DashboardController@index')->name('dashboard');
    Route::get('/data-pengajar', 'AdminController\DashboardController@dataPengajar')->name('data-pengajar');
    Route::get('/data-kelas', 'AdminController\DashboardController@dataKelas')->name('data-kelas');
    Route::get('/data-kategori', 'AdminController\DashboardController@dataKategori')->name('data-kategori');
    
    Route::get('/kelas', 'AdminController\KelasController@create')->name('tambah-kelas');
    Route::post('/kelas', 'AdminController\KelasController@store');
    Route::get('/kelas/{id}', 'AdminController\KelasController@edit')->name('edit-kelas');
    Route::post('/kelas/{id}', 'AdminController\KelasController@update');
    Route::get('/kelas/{id}/delete', 'AdminController\KelasController@destroy')->name('hapus-kelas');

    Route::get('/pengajar', 'AdminController\PengajarController@create')->name('tambah-pengajar');
    Route::post('/pengajar', 'AdminController\PengajarController@store');
    Route::get('/pengajar/{id}', 'AdminController\PengajarController@edit')->name('edit-pengajar');
    Route::post('/pengajar/{id}', 'AdminController\PengajarController@update');
    Route::get('/pengajar/{id}/delete', 'AdminController\PengajarController@destroy')->name('hapus-pengajar');

    Route::get('/kategori', 'AdminController\KategoriController@create')->name('tambah-kategori');
    Route::post('/kategori', 'AdminController\KategoriController@store');
    Route::get('/kategori/{id}', 'AdminController\KategoriController@edit')->name('edit-kategori');
    Route::post('/kategori/{id}', 'AdminController\KategoriController@update');
    Route::get('/kategori/{id}/delete', 'AdminController\KategoriController@destroy')->name('hapus-kategori');
});

Route::get('/', 'UserController\DashboardController@index')->name('dashboard-user');
Route::get('/kelas/{id}', 'UserController\DashboardController@show')->name('kelas');
Route::get('/logout', 'AuthController@logout')->name('logout');