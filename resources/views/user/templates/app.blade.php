<!doctype html>
<html lang="en">
@include('user.templates.head')
<body>
@include('user.templates.navbar')
<div class="container">

@yield('content')
</div>

@include('user.templates.foot')
</body>
</html>