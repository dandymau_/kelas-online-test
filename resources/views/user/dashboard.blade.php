@extends('user.templates.app')

@section('content')
<div class="row my-4">
    @foreach ($data_kelas as $kelas)
    <div class="col-sm-3">
        <div class="card">
            <div class="card-body">
                <h5 class="card-title">{{$kelas->nama_kelas}}</h5>
                <h6 class="card-subtitle mb-2 text-muted">{{$kelas->pengajar_kelas->nama_pengajar}}</h6>
                <p class="card-text">{{$kelas->deskripsi}}</p>
                <a href="{{route('kelas', ['id' => $kelas->id])}}" class="card-link">Detail</a>
            </div>
        </div>
    </div>
    @endforeach
@endsection
