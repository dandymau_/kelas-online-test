@extends('user.templates.app')

@section('content')
<h1>{{$kelas->nama_kelas}}</h1>
<h3>{{$kelas->pengajar_kelas->nama_pengajar}}</h3>
<div class="row my-5">
    <div class="col">
        <video width="340" controls>
            <source src="{{asset($kelas->trailer)}}" type="video/mp4">
            Your browser does not support HTML video.
        </video>
    </div>
    <div class="col">
        <p>{{$kelas->deskripsi}}</p>
    </div>
</div>
@endsection