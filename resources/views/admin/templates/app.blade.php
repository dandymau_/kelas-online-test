<!doctype html>
<html lang="en">
@include('admin.templates.head')
<body>
@include('admin.templates.navbar')
<div class="container">

@yield('content')
</div>

@include('admin.templates.foot')
</body>
</html>