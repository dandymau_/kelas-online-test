@extends('admin.templates.app')

@section('content')
<h1>Edit Pengajar</h1>
<form method="post" action="{{route('edit-pengajar', ['id' => $pengajar->id])}}" enctype="multipart/form-data">
    @csrf
    <div class="form-group">
        <label for="nama_pengajar">Nama Pengajar</label>
        <input type="text" class="form-control" name="nama_pengajar" id="nama_pengajar" value="{{$pengajar->nama_pengajar}}">
    </div>
    <input type="submit" class="btn btn-primary btn-md" value="Edit Pengajar" style="float:right">
</form>
@endsection

@push('scripts')
<script src="{{asset('admin/plugins/bs-custom-file-input/bs-custom-file-input.min.js')}}"></script>
<script>
    $(document).ready(function () {
        bsCustomFileInput.init();
    });
</script>
@endpush