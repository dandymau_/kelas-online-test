@extends('admin.templates.app')

@section('content')
<h1>Tambah Kelas</h1>
<form method="post" action="{{route('tambah-kelas')}}" enctype="multipart/form-data">
    @csrf
    <div class="form-group">
        <label for="nama_kelas">Nama Kelas</label>
        <input type="text" class="form-control" name="nama_kelas" id="nama_kelas" placeholder="Contoh: Kelas Memasak bersama Chef Juna">
    </div>
    <div class="form-group">
        <label for="pengajar">Pengajar</label>
        <select class="form-control" id="pengajar" name="pengajar">
            <option disabled selected>Pilih Pengajar</option>
            @foreach ($data_pengajar as $pengajar)
            <option value="{{$pengajar->id}}">{{$pengajar->nama_pengajar}}</option>
            @endforeach
        </select>
    </div>
    <div class="form-group">
        <label for="kategori">Kategori</label>
        <select class="form-control" id="kategori" name="kategori">
            <option disabled selected>Pilih Kategori</option>
            @foreach ($data_kategori as $kategori)
            <option value="{{$kategori->id}}">{{$kategori->nama_kategori}}</option>
            @endforeach
        </select>
    </div>
    <div class="form-group">
        <label for="trailer">Trailer</label>
        <div class="custom-file">
            <input type="file" class="custom-file-input" id="trailer" name="trailer">
            <label class="custom-file-label" for="trailer">Pilih file</label>
        </div>
    </div>
    <div class="form-group">
        <label for="deskripsi">Deskripsi</label>
        <textarea class="form-control" id="deskripsi" name="deskripsi" rows="3"></textarea>
    </div>
    <input type="submit" class="btn btn-primary btn-md" value="Tambah Kelas" style="float:right">
</form>
@endsection

@push('scripts')
<script src="{{asset('admin/plugins/bs-custom-file-input/bs-custom-file-input.min.js')}}"></script>
<script>
    $(document).ready(function () {
        bsCustomFileInput.init();
    });
</script>
@endpush