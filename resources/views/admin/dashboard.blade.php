@extends('admin.templates.app')

@push('styles')
<link rel="stylesheet" href="//cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css">
@endpush

@section('content')
<div class="card my-2">
    <div class="card-header">
        <div class="d-flex justify-content-between">
            <div>
                Data Kelas
            </div>
            <div>
                <a href="{{route('tambah-kelas')}}" class="btn btn-primary btn-sm">Tambah Kelas</a>
            </div>
        </div>
    </div>
    <div class="card-body">
        <div>
            <table class="table table-bordered" id="kelas-table">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Kelas</th>
                        <th>Pengajar</th>
                        <th>Deskripsi</th>
                        <th width="100px">Action</th>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <th>No</th>
                        <th>Kelas</th>
                        <th>Pengajar</th>
                        <th>Deskripsi</th>
                        <th width="100px">Action</th>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
</div>

<div class="card my-2">
    <div class="card-header">
        <div class="d-flex justify-content-between">
            <div>
                Data Pengajar
            </div>
            <div>
                <a href="{{route('tambah-pengajar')}}" class="btn btn-primary btn-sm">Tambah Pengajar</a>
            </div>
        </div>
    </div>
    <div class="card-body">
        <div>
            <table class="table table-bordered" id="pengajar-table">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Nama</th>
                        <th width="100px">Action</th>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <th>No</th>
                        <th>Nama</th>
                        <th width="100px">Action</th>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
</div>

<div class="card my-2">
    <div class="card-header">
        <div class="d-flex justify-content-between">
            <div>
                Data Kategori
            </div>
            <div>
                <a href="{{route('tambah-kategori')}}" class="btn btn-primary btn-sm">Tambah Kategori</a>
            </div>
        </div>
    </div>
    <div class="card-body">
        <div>
            <table class="table table-bordered" id="kategori-table">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Kategori</th>
                        <th width="100px">Action</th>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <th>No</th>
                        <th>Kelas</th>
                        <th width="100px">Action</th>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
</div>


@endsection

@push('scripts')
<script src="https://cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>
<script>
    $(document).ready(function () {
        $('#kelas-table').DataTable({
            processing: true,
            serverSide: true,
            ajax: "/admin-page/data-kelas",
            columns: [{
                    data: 'DT_RowIndex',
                    name: 'DT_RowIndex'
                },
                {
                    data: 'nama_kelas',
                    name: 'nama_kelas'
                },
                {
                    data: 'nama_pengajar',
                    name: 'nama_pengajar'
                },
                {
                    data: 'deskripsi',
                    name: 'deskripsi'
                },
                {
                    data: 'action',
                    name: 'action',
                    orderable: false,
                    searchable: false
                }
            ]
        });

        $('#pengajar-table').DataTable({
            processing: true,
            serverSide: true,
            ajax: '/admin-page/data-pengajar',
            columns: [{
                    data: 'DT_RowIndex',
                    name: 'DT_RowIndex'
                },
                {
                    data: 'nama_pengajar',
                    name: 'nama_pengajar'
                },
                {
                    data: 'action',
                    name: 'action'
                }
            ]
        });

        $('#kategori-table').DataTable({
            processing: true,
            serverSide: true,
            ajax: '/admin-page/data-kategori',
            columns: [{
                    data: 'DT_RowIndex',
                    name: 'DT_RowIndex'
                },
                {
                    data: 'nama_kategori',
                    name: 'nama_kategori'
                },{
                    data: 'action',
                    name: 'action'
                }
            ]
        });
    })

</script>
@endpush
