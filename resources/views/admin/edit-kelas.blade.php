@extends('admin.templates.app')

@section('content')
<h1>Edit Kelas</h1>
<form method="post" action="{{route('edit-kelas', ['id' => $kelas->id])}}" enctype="multipart/form-data">
    @csrf
    <div class="form-group">
        <label for="nama_kelas">Nama Kelas</label>
        <input type="text" class="form-control" name="nama_kelas" id="nama_kelas" placeholder="Contoh: Kelas Memasak bersama Chef Juna" value="{{$kelas->nama_kelas}}">
    </div>
    <div class="form-group">
        <label for="pengajar">Pengajar</label>
        <select class="form-control" id="pengajar" name="pengajar">
            @foreach ($data_pengajar as $pengajar)
            @if ($pengajar->id == $kelas->pengajar)
            <option value="{{$pengajar->id}}" selected>{{$pengajar->nama_pengajar}}</option>
            @else
            <option value="{{$pengajar->id}}">{{$pengajar->nama_pengajar}}</option>
            @endif
            @endforeach
        </select>
    </div>
    <div class="form-group">
        <label for="kategori">Kategori</label>
        <select class="form-control" id="kategori" name="kategori">
            @foreach ($data_kategori as $kategori)
            @if ($kategori->id == $kelas->kategori)
            <option value="{{$kategori->id}}" selected>{{$kategori->nama_kategori}}</option>
            @else
            <option value="{{$kategori->id}}">{{$kategori->nama_kategori}}</option>
            @endif
            @endforeach
        </select>
    </div>
    <video width="320" height="240" controls>
        <source src="{{asset($kelas->trailer)}}" type="video/mp4">
        Your browser does not support the video tag.
    </video>
    <div class="form-group">
        <label for="trailer">Trailer</label>
        <div class="custom-file">
            <input type="file" class="custom-file-input" id="trailer" name="trailer">
            <label class="custom-file-label" for="trailer">{{$kelas->trailer}}</label>
        </div>
    </div>
    <div class="form-group">
        <label for="deskripsi">Deskripsi</label>
        <textarea class="form-control" id="deskripsi" name="deskripsi" rows="3">{{$kelas->deskripsi}}</textarea>
    </div>
    <input type="submit" class="btn btn-primary btn-md" value="Edit Kelas" style="float:right">
</form>
@endsection

@push('scripts')
<script src="{{asset('admin/plugins/bs-custom-file-input/bs-custom-file-input.min.js')}}"></script>
<script>
    $(document).ready(function () {
        bsCustomFileInput.init();
    });
</script>
@endpush