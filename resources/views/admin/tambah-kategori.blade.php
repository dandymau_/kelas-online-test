@extends('admin.templates.app')

@section('content')
<h1>Tambah Kategori</h1>
<form method="post" action="{{route('tambah-kategori')}}" enctype="multipart/form-data">
    @csrf
    <div class="form-group">
        <label for="nama_kategori">Nama Kategori</label>
        <input type="text" class="form-control" name="nama_kategori" id="nama_kategori" placeholder="Contoh: Memasak">
    </div>
    <input type="submit" class="btn btn-primary btn-md" value="Tambah Kategori" style="float:right">
</form>
@endsection

@push('scripts')
<script src="{{asset('admin/plugins/bs-custom-file-input/bs-custom-file-input.min.js')}}"></script>
<script>
    $(document).ready(function () {
        bsCustomFileInput.init();
    });
</script>
@endpush