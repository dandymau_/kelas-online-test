@extends('admin.templates.app')

@section('content')
<h1>Tambah Pengajar</h1>
<form method="post" action="{{route('tambah-pengajar')}}" enctype="multipart/form-data">
    @csrf
    <div class="form-group">
        <label for="nama_pengajar">Nama Pengajar</label>
        <input type="text" class="form-control" name="nama_pengajar" id="nama_pengajar" placeholder="Contoh: Chef Juna">
    </div>
    <input type="submit" class="btn btn-primary btn-md" value="Tambah Pengajar" style="float:right">
</form>
@endsection

@push('scripts')
<script src="{{asset('admin/plugins/bs-custom-file-input/bs-custom-file-input.min.js')}}"></script>
<script>
    $(document).ready(function () {
        bsCustomFileInput.init();
    });
</script>
@endpush