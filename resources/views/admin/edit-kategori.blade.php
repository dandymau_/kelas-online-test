@extends('admin.templates.app')

@section('content')
<h1>Edit Kategori</h1>
<form method="post" action="{{route('edit-kategori', ['id' => $kategori->id])}}" enctype="multipart/form-data">
    @csrf
    <div class="form-group">
        <label for="nama_kategori">Nama Kategori</label>
        <input type="text" class="form-control" name="nama_kategori" id="nama_kategori" value="{{$kategori->nama_kategori}}">
    </div>
    <input type="submit" class="btn btn-primary btn-md" value="Edit Kategori" style="float:right">
</form>
@endsection

@push('scripts')
<script src="{{asset('admin/plugins/bs-custom-file-input/bs-custom-file-input.min.js')}}"></script>
<script>
    $(document).ready(function () {
        bsCustomFileInput.init();
    });
</script>
@endpush